from pydantic import BaseModel


class CustomerReceiptDto(BaseModel):
    id: int
    user_id: int
    lastname: str
    email: str
    price: float
