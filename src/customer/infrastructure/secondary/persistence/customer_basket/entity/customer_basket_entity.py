from sqlalchemy import Column, Integer, Float
from sqlalchemy.orm import declarative_base

CustomerBasketEntityBase = declarative_base()


class CustomerBasketEntity(CustomerBasketEntityBase):
    __tablename__ = "customer_basket"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, unique=True, index=True)
    price = Column(Float, index=True, nullable=True)
