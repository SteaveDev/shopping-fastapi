from sqlalchemy.orm import Session

from src.customer.domain.model.customer import Customer
from src.customer.domain.spi.customer_spi import CustomerSpi
from src.customer.infrastructure.secondary.persistence.customer.entity.customer_entity import CustomerEntity
from src.customer.infrastructure.secondary.persistence.customer.mapper.customer_entity_mapper import \
    CustomerEntityMapper


class CustomerAdapter(CustomerSpi):
    def __init__(self, session: Session):
        self.session = session

    def get_all_customers(self) -> list[Customer]:
        customer_entities = self.session.query(CustomerEntity).all()

        customers = [CustomerEntityMapper.to_model(entity) for entity in customer_entities]
        return customers
