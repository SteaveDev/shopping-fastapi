from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.customer.infrastructure.secondary.persistence.customer.entity.customer_entity import CustomerEntityBase
from src.customer.infrastructure.secondary.persistence.customer_basket.entity.customer_basket_entity import \
    CustomerBasketEntityBase

SQLALCHEMY_DATABASE_URL = "postgresql://admin:123456@localhost:5431/shopping"
engine = create_engine(SQLALCHEMY_DATABASE_URL)

CustomerEntityBase.metadata.create_all(bind=engine)
CustomerBasketEntityBase.metadata.create_all(bind=engine)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def repository():
    postgresql_database = SessionLocal()
    try:
        yield postgresql_database
    finally:
        postgresql_database.close()
