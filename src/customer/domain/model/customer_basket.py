class CustomerBasket:
    def __init__(self, id: int, user_id: int, price: float):
        self.id = id
        self.user_id = user_id
        self.price = price

    def __eq__(self, other):
        if not isinstance(other, CustomerBasket):
            return False
        return (
                self.id == other.id and
                self.user_id == other.user_id and
                self.price == other.price
        )
