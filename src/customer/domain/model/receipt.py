class CustomerReceipt:
    def __init__(self, id: int, user_id: int, lastname: str, email: str, price: float):
        self.id = id
        self.user_id = user_id
        self.lastname = lastname
        self.email = email
        self.price = price

    def __eq__(self, other):
        if not isinstance(other, CustomerReceipt):
            return False
        return (
                self.id == other.id and
                self.user_id == other.user_id and
                self.lastname == other.lastname and
                self.email == other.email and
                self.price == other.price
        )
