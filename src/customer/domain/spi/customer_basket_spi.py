from typing import List

from src.customer.domain.model.customer_basket import CustomerBasket


class CustomerBasketSpi:

    def get_all_customer_baskets(self) -> List[CustomerBasket]:
        pass
