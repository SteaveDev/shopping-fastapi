from typing import List

from src.customer.domain.model.customer import Customer


class CustomerSpi:

    def get_all_customers(self) -> List[Customer]:
        pass
