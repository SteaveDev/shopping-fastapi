from typing import List

from src.customer.domain.model.receipt import CustomerReceipt


class GetCustomersUseCase:
    def execute(self) -> List[CustomerReceipt]:
        pass
